const express = require('express')
const app = express()

process.env.PORT = 3000

app.disable('x-powered-by')

app.use('/check', (req, res) => {
  res.send('App running succesfully')
})

app.get('/', (req, res) => {
  res.send({
    status: 'OK',
    payload: 'Hello World'
  })
})

app.get('/me', (req, res) => {
  res.send({
    status: 'OK',
    payload: 'Hello Ganny'
  })
})

app.listen(process.env.PORT, () => {
  console.log(`app running ${process.env.PORT}`)
})
